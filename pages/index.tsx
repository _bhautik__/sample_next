import type { NextPage } from "next"
import { useRouter } from "next/router"
import styles from "../styles/Home.module.css"

const Home: NextPage = () => {
  const router = useRouter()
  const about = () => {
    router.push("/about")
  }
  return (<>
    <div className={styles.container}>
      home page view
      <br />
      <button onClick={about}>about-page</button>
      <button onClick={about}>about-page</button>
      <br />
      <button onClick={about}>about-page</button>
    </div>
  </>
  )
}

export default Home
